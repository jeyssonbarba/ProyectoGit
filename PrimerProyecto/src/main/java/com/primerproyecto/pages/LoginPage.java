package com.primerproyecto.pages;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://sahitest.com/demo/training/login.htm")
public class LoginPage extends PageObject {

	@FindBy(name = "user")
	WebElementFacade Txt_usuario;

	@FindBy(name = "password")
	WebElementFacade Txt_password;

	@FindBy(xpath = "/html/body/center/div/form/table/tbody/tr[3]/td[2]/input")
	WebElementFacade Btn_login;
	
	@FindBy(xpath = "//*[@id='available']/h2")
	WebElementFacade Lbl_inicio;

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public void login() {
		Txt_usuario.sendKeys("test");
		Txt_password.sendKeys("secret");
		Btn_login.click();
	}
	
	public void validarIngresi(){
		MatcherAssert.assertThat("Usuario y clave incorrecto", Lbl_inicio.isDisplayed());
	}
	
	

}
