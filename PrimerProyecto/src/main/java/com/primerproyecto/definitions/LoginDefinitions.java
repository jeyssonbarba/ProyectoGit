package com.primerproyecto.definitions;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.primerproyecto.steps.LoginSteps;

import net.thucydides.core.annotations.Steps;

public class LoginDefinitions {
	
	@Steps
	LoginSteps pasos;
	
	@Given("que estoy en la aplicacion de training site")
	public void iniciarUrl(){
		pasos.abrirUrl();
	}
	
	@When("inicio sesion")
	public void iniciarSesion(){
		pasos.login();
		
	}
	
	@Then("acceso a comprar libros")
	public void ValidarInicio(){
		pasos.validarMensaje();
	}

}
