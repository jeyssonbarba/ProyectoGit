package com.primerproyecto.steps;

import org.fluentlenium.core.annotation.Page;

import com.primerproyecto.pages.LoginPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class LoginSteps {

	@Page
	LoginPage login;
	
	@Step
	public void abrirUrl(){
		login.open();
	}
	
	@Step
	public void login(){
		login.login();
	}
	
	@Step
	public void validarMensaje(){
		login.validarIngresi();
	}
}
